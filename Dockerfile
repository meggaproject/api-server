FROM python:3.10.13-slim

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN apt-get update \
    && apt-get --no-install-recommends -y install libpq-dev gcc python3-dev
RUN pip install --no-cache-dir -r requirements.txt
COPY . .

CMD [ "uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000" ]
