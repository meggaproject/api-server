import pytest
from app import schemas


def test_get_all_requests(authorized_client, test_requests):
    res = authorized_client.get("/requests/")
    # assert len(res.json()) == len(test_requests)
    assert res.status_code == 200


def test_unauthorized_user_get_all_requests(client, test_requests):
    res = client.get("/requests/")
    assert res.status_code == 401


def test_unauthorized_user_get_one_request(client, test_requests):
    res = client.get(f"/requests/{test_requests[0].id}")
    assert res.status_code == 401


def test_get_one_request_not_exist(authorized_client, test_requests):
    res = authorized_client.get(f"/requests/88888")
    assert res.status_code == 404


def test_get_one_request(authorized_client, test_requests):
    res = authorized_client.get(f"/requests/{test_requests[0].id}")
    request = schemas.RequestOut(**res.json())
    assert request.request_detail.id == test_requests[0].id
    assert request.request_detail.name == test_requests[0].name
    assert request.request_detail.video_url == test_requests[0].video_url


@pytest.mark.parametrize("name, video_url, processed", [
    ("supervideo", "https://video.com", False),
    ("golang", "https://toplang.com", True),
    ("devops", "https://rulezz.com", False),
])
def test_create_request(authorized_client, test_user, test_requests, name, video_url, processed):
    res = authorized_client.post(
        "/requests/", json={"name": name, "video_url": video_url, "processed": processed})

    created_request = schemas.Request(**res.json())
    assert res.status_code == 201
    assert created_request.name == name
    assert created_request.video_url == video_url
    assert created_request.processed == processed
    assert created_request.owner_id == test_user['id']


def test_create_request_default_processed_false(authorized_client, test_user, test_requests):
    res = authorized_client.post(
        "/requests/", json={"name": "some name", "video_url": "https://foo.bar"})

    created_request = schemas.Request(**res.json())
    assert res.status_code == 201
    assert created_request.name == "some name"
    assert created_request.video_url == "https://foo.bar"
    assert created_request.processed == False
    assert created_request.owner_id == test_user['id']


def test_unauthorized_user_create_request(client, test_user, test_requests):
    res = client.post(
        "/requests/", json={"name": "some name", "video_url": "https://foo.bar"})
    assert res.status_code == 401


def test_unauthorized_user_delete_request(client, test_user, test_requests):
    res = client.delete(
        f"/requests/{test_requests[0].id}")
    assert res.status_code == 401


def test_delete_request_success(authorized_client, test_user, test_requests):
    res = authorized_client.delete(
        f"/requests/{test_requests[0].id}")

    assert res.status_code == 204


def test_delete_request_non_exist(authorized_client, test_user, test_requests):
    res = authorized_client.delete(
        f"/requests/8000000")

    assert res.status_code == 404


def test_delete_other_user_request(authorized_client, test_user, test_requests):
    res = authorized_client.delete(
        f"/requests/{test_requests[3].id}")
    assert res.status_code == 403


def test_update_request(authorized_client, test_user, test_requests):
    data = {
        "name": "updated name",
        "video_url": "updatd url",
        "id": test_requests[0].id

    }
    res = authorized_client.put(f"/requests/{test_requests[0].id}", json=data)
    updated_request = schemas.Request(**res.json())
    assert res.status_code == 200
    assert updated_request.name == data['name']
    assert updated_request.video_url == data['video_url']


def test_update_other_user_request(authorized_client, test_user, test_user2, test_requests):
    data = {
        "name": "updated name",
        "video_url": "updated url",
        "id": test_requests[3].id

    }
    res = authorized_client.put(f"/requests/{test_requests[3].id}", json=data)
    assert res.status_code == 403


def test_unauthorized_user_update_request(client, test_user, test_requests):
    res = client.put(
        f"/requests/{test_requests[0].id}")
    assert res.status_code == 401


def test_update_request_non_exist(authorized_client, test_user, test_requests):
    data = {
        "name": "updated name",
        "video_url": "updatd url",
        "id": test_requests[3].id

    }
    res = authorized_client.put(
        f"/requests/8000000", json=data)

    assert res.status_code == 404
