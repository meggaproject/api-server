from pydantic import BaseModel, EmailStr
from datetime import datetime
from typing import Optional


class RequestBase(BaseModel):
    name: str
    description: Optional[str] = None
    video_url: str
    text_url: Optional[str] = None
    processed: bool = False
    archived: bool = False

class CreateRequest(RequestBase):
    pass

class CreateUser(BaseModel):
    email: EmailStr
    password: str

class UserOut(BaseModel):
    id: int
    email: EmailStr
    created_at: datetime
    class Config:
        from_attributes = True

class UserLogin(BaseModel):
    email: EmailStr
    password: str


class Request(RequestBase):
    id: int
    created_at: datetime
    updated_at: Optional[bool] = None
    owner_id: int
    owner: UserOut
    class Config:
        from_attributes = True

class RequestOut(BaseModel):
    request_detail: Request

class Token(BaseModel):
    access_token: str
    token_type: str

class TokenData(BaseModel):
    id: Optional[int] = None
