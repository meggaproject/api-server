from .. import models, schemas, oauth2
from fastapi import FastAPI, Response, status, HTTPException, Depends, APIRouter
from sqlalchemy.orm import Session
from ..database import get_db
from typing import List, Optional


router = APIRouter(prefix="/requests", tags=["Requests"])

@router.get("/", response_model=List[schemas.Request])
def get_requests(db: Session = Depends(get_db), current_user: int = Depends(oauth2.get_current_user), limit: int = 10, skip: int = 0, search: Optional[str] = ""):
    # requests = db.query(models.Request).all()
    # skip used for pagination
    requests = db.query(models.Request).filter(models.Request.owner_id == current_user.id).filter(models.Request.name.contains(search)).limit(limit).offset(skip).all()
    return requests

@router.post("/", status_code=status.HTTP_201_CREATED, response_model=schemas.Request)
def create_request(request: schemas.CreateRequest, db: Session = Depends(get_db), current_user: int = Depends(oauth2.get_current_user)):
    new_request = models.Request(owner_id=current_user.id, **request.model_dump())
    db.add(new_request)
    db.commit()
    db.refresh(new_request)
    return new_request

@router.get("/{id}", response_model=schemas.RequestOut)
def get_request(id: int, db: Session = Depends(get_db), current_user: int = Depends(oauth2.get_current_user)):
    request = db.query(models.Request).filter(models.Request.id == id).first()
    
    if not request:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"request with id: {id} was not found")
    
    if request.owner_id != current_user.id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Not authorize to perform requested action")
    
    return {"request_detail": request}


@router.delete("/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_request(id: int, db: Session = Depends(get_db), current_user: int = Depends(oauth2.get_current_user)):
    request_query = db.query(models.Request).filter(models.Request.id == id)
    request = request_query.first()

    if request == None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"request with id: {id} was not found")
    
    if request.owner_id != current_user.id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Not authorize to perform requested action")
    
    request_query.delete(synchronize_session=False)
    db.commit()
    return Response(status_code=status.HTTP_204_NO_CONTENT)

@router.put("/{id}", response_model=schemas.Request)
def update_request(id: int, updated_request:schemas.CreateRequest, db: Session = Depends(get_db), current_user: int = Depends(oauth2.get_current_user)):
    request_query = db.query(models.Request).filter(models.Request.id == id)
    request = request_query.first()

    if request == None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"request with id: {id} was not found")
    
    if request.owner_id != current_user.id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Not authorize to perform requested action")
    
    request_query.update(updated_request.model_dump(), synchronize_session=False)
    db.commit()
    return request_query.first()
