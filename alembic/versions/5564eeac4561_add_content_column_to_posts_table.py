"""add content column to posts table

Revision ID: 5564eeac4561
Revises: 5831c3242f24
Create Date: 2024-01-23 14:36:26.848845

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '5564eeac4561'
down_revision: Union[str, None] = '5831c3242f24'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column('posts', sa.Column('content', sa.String(), nullable=False))
    pass


def downgrade() -> None:
    op.drop_column('posts', 'content')
    pass
