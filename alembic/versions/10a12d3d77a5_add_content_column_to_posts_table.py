"""add content column to posts table

Revision ID: 10a12d3d77a5
Revises: 5564eeac4561
Create Date: 2024-01-23 14:37:16.649374

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '10a12d3d77a5'
down_revision: Union[str, None] = '5564eeac4561'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
